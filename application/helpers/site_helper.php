<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function slug($string, $spaceRepl = "-")
{
    $string = str_replace("&", "and", $string);

    $string = preg_replace("/[^a-zA-Z0-9 _-]/", "", $string);

    $string = strtolower($string);

    $string = preg_replace("/[ ]+/", " ", $string);

    $string = str_replace(" ", $spaceRepl, $string);

    return $string;
}
function sqlval ($query) {
  $query = mysql_real_escape_string($query);
  return $query;
}

function uploadfoto($file,$location,$slug) {
  $target_dir = "assets/img/".$location."/";
  $target_file = $target_dir . $slug.'.jpg';
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Check if image file is a actual image or fake image
  if(isset($_POST["submit"])) {
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
      return "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
    } else {
      return "File is not an image.";
      $uploadOk = 0;
    }
  }

  // Check if file already exists
  if (file_exists($target_file)) {
    // return "Sorry, file already exists.";
    // $uploadOk = 0;
  }

  // Check file size
  if ($file["size"] > 5000000) {
    return "Sorry, your file is too large.";
    $uploadOk = 0;
  }

  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
    return "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
  }

  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
    return "Sorry, your file was not uploaded.";
  // if everything is ok, try to upload file
  } else {
    if (move_uploaded_file($file["tmp_name"], $target_file)) {
      return "success";
    } else {
      return "Sorry, there was an error uploading your file.";
    }
  }
}

function checkRemoteFile($url)
{
	if (is_file($url)) { return false; } else { return true;}
}

function cekfoto ($lokasi) {
  $tambahan = '';
  if (strpos($lokasi, '/thumb/') !== false) {
    $tambahan = 'thumb/';
  } else if (strpos($lokasi, '/thumb_mini/') !== false) {
    $tambahan = 'thumb_mini/';
  }
  if (checkRemoteFile(URLUTAMA.$lokasi)) return URLUTAMA.$lokasi; else return URLUTAMA.'img/'.$tambahan.'produk-default.jpg';
}
