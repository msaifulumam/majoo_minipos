
	</div>
</div>
<!-- jQuery -->
<script src="<?=URLUTAMA?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=URLUTAMA?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="text/javascript">
function imageExists(image_url){

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}
function jadirupiah(x) {
    return 'Rp. '+x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
}
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=URLUTAMA?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=URLUTAMA?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=URLUTAMA?>assets/plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=URLUTAMA?>assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="<?=URLUTAMA?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=URLUTAMA?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="<?=URLUTAMA?>assets/plugins/select2/js/select2.full.min.js"></script>
<!-- daterangepicker -->
<script src="<?=URLUTAMA?>assets/plugins/moment/moment.min.js"></script>
<script src="<?=URLUTAMA?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=URLUTAMA?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=URLUTAMA?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=URLUTAMA?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?=URLUTAMA?>assets/js/jquery.fancybox.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=URLUTAMA?>assets/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=URLUTAMA?>assets/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-danger">
      <div class="modal-header">
        <h4 class="modal-title">Gagal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body keterangan">
        <p>Keterangan</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">OK</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-success" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-success">
      <div class="modal-header">
        <h4 class="modal-title">Berhasil</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body keterangan">
        <p>Keterangan</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">OK</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-konfirmasi" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-default">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body keterangan">
        <p>Keterangan</p>
      </div>
      <div class="modal-footer justify-content-between">
				<button type="button" class="btnbatal btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btnok btn btn-danger" data-dismiss="modal">Hapus</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
</body>
</html>
