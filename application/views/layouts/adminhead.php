<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Majoo Minipos</title>
	<!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/select2/css/select2.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=URLUTAMA?>assets/plugins/summernote/summernote-bs4.min.css">
	<link href="<?=URLUTAMA?>assets/css/jquery.fancybox.min.css" rel="stylesheet">

	<style type="text/css">


	</style>
</head>
<body class="sidebar-mini layout-fixed">
<?php
$menusamping = [
	'dashboard' => ['icon'=>'tachometer-alt','judul'=>'Dashboard','link'=>''],
	'Kategori' => ['icon'=>'database','judul'=>'Kategori','link'=>'adminkategori'],
	'Produk' => ['icon'=>'database','judul'=>'Data Produk','link'=>'adminproduk'],
	'Pembeli' => ['icon'=>'user','judul'=>'Pembeli','link'=>'adminpembeli'],
	'Supplier' => ['icon'=>'users','judul'=>'Supplier','link'=>'adminsupplier'],
	'Logout' => ['icon'=>'circle text-danger','judul'=>'Logout','link'=>'api/logout'],
];
if ( ! $this->session->userdata('logged_in')) {
	$menusamping=[];
}
?>
<div class="wrapper">
	<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

    </ul>
  </nav>
	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<!-- Brand Logo -->
		<a href="<?=URLUTAMA?>" class="brand-link">
			<img src="<?=URLUTAMA?>assets/img/majoo.png"class="brand-image img-circle elevation-3" style="opacity: .8">
			<span class="brand-text font-weight-light">MiniPOS</span>
		</a>

		<!-- Sidebar -->
		<div class="sidebar">
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel mt-3 pb-3 mb-3 d-flex">
				<?php if ( $this->session->userdata('logged_in')) { ?>
				<div class="image">
					<img src="<?=URLUTAMA?>assets/img/pengguna/<?=$this->session->userdata('id')?>.jpg" class="img-circle elevation-2" alt="User Image">
				</div>
				<div class="info">
					<a href="#" class="d-block"><?=$this->session->userdata('username')?></a>
				</div>
			<?php } ?>
			</div>



			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					<!-- Add icons to the links using the .nav-icon class
							 with font-awesome or any other icon font library -->
							 <?php foreach ($menusamping as $key => $value) {
	 							echo '<li class="nav-item">
	 								<a href="'.URLUTAMA.$value["link"].'" class="nav-link">
	 									<i class="nav-icon fas fa-'.$value["icon"].'"></i>
	 									<p>'.$value["judul"].'</p>
	 								</a>
	 							</li>';
	 						}
							?>
				</ul>
			</nav>
			<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
