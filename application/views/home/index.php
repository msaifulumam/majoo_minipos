<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
			<div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info boxkategori">
              <div class="inner">
                <h3>150</h3>
                <p>Kategori</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?=URLUTAMA?>adminkategori" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6 boxproduk">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53</h3>

                <p>Produk</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?=URLUTAMA?>adminproduk" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning boxpembeli">
              <div class="inner">
                <h3>44</h3>

                <p>Pembeli</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="<?=URLUTAMA?>adminpembeli" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger boxsupplier">
              <div class="inner">
                <h3>65</h3>

                <p>Supplier</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="<?=URLUTAMA?>adminsupplier" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
				<h2 class="m-0">Produk Terbaru</h2>
				<div class="row" id="listproduk">


				</div>

		</section>
<script type="text/javascript">
function updatedatakategori(filter='') {
  $.post( "<?=URLUTAMA?>api/kategori_getall", { data: [''], filter: filter })
  .done(function( data ) { $('.boxkategori h3').html(JSON.parse(data)); });
}
function updatedataproduk(filter='') {
  $.post( "<?=URLUTAMA?>api/produk_getall", { data: [''], filter: filter })
  .done(function( data ) { $('.boxproduk h3').html(JSON.parse(data)); });
}
function updatedatapembeli(filter='') {
  $.post( "<?=URLUTAMA?>api/pembeli_getall", { data: [''], filter: filter })
  .done(function( data ) { $('.boxpembeli h3').html(JSON.parse(data)); });
}
function updatedatasupplier(filter='') {
  $.post( "<?=URLUTAMA?>api/supplier_getall", { data: [''], filter: filter })
  .done(function( data ) { $('.boxsupplier h3').html(JSON.parse(data)); });
}
document.addEventListener("DOMContentLoaded", function(event) {
  updatedatakategori();
	updatedataproduk();
	updatedatapembeli();
	updatedatasupplier();
	produk_load();

});

function produk_load(page=1,perpage=4,filter='1 ORDER BY created_at DESC') {
  $.post( "<?=URLUTAMA?>api/produk_get", {
    data: [''],
    select: ['id','nama','deskripsi','harga','slug'],
    filter: filter,
    page: page-1,
    perpage: perpage
  })
  .done(function( data ) {
    data = JSON.parse(data);
    var html = '';
    pagenow=page, perpagenow=perpage;
    $.each( data, function( i, item ) {
      var linkimg = '<?=URLUTAMA?>assets/img/produk/'+item.slug+'.jpg';
      var imsrc = imageExists(linkimg)?linkimg:'<?=URLUTAMA?>assets/img/produk-default.jpg';
      html += '<div class="col-6 col-lg-3"><div class="card card-success"><div class="card-header"><h3 class="card-title">'+item.nama+'</h3></div><div class="card-body"><a rel="galeriproduk" data-fancybox="galeriproduk" class="thumb" href="'+imsrc+'"><img src="'+imsrc+'" style="width:100%" /></a><h4>'+jadirupiah(item.harga)+'</h4><p>'+item.deskripsi+'</p><a class="btn btn-success" href="<?=URLUTAMA?>beli/'+item.slug+'" >BELI</a></div></div></div>';
    });
    $('#listproduk').html(html);
		jQuery(function ($) {
			o=$(".thumb"),o.length>0&&o.fancybox({'transitionIn':'elastic','transitionOut':'elastic','speedIn':600,'speedOut':200,'overlayShow':false});
		});
  });
}
</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
