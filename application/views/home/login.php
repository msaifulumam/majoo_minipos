<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Login</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
			<div class="row">
          <div class="col-12">
            <!-- small box -->
						<form id="login" enctype='multipart/form-data'>
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" class="form-control" id="username" placeholder="Masukkan Nama Produk" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Masukkan Deskripsi" required>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" class="btn btn-primary" onclick="login()">Login</button>
              </div>
            </form>
          </div>

        </div>
		</section>
		<p>Note: Username : saiful, password : saiful</p>
<script type="text/javascript">
function login() {
	if ($('#nama').val()!='' && $('#password').val()!='') {
		var data = new FormData();
		data.append('username',$('#username').val());
		data.append('password', $('#password').val());

	$.ajax({
				url: "<?=URLUTAMA?>api/login",
				type: 'post',
				data: data,
				contentType: false,
				processData: false,
				success: function(data){
					 if (data==1 || data=='success') {$('#modal-success .keterangan>p').html('Login Berhasil'); $('#modal-success').modal(); $("#modal-success").on("hidden.bs.modal", function () {location.href = '<?=URLUTAMA?>'; });
				 } else { $('#modal-danger .keterangan>p').html('Nama atau Password Salah'); $('#modal-danger').modal(); }
				},
		 });
	 }else{
			$('#modal-danger .keterangan>p').html('Nama dan Password Tidak Boleh Kosong'); $('#modal-danger').modal();
	 }
}
document.addEventListener("DOMContentLoaded", function(event) {

});
</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
