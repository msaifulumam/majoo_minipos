<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ubah Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->

          </div>
				</div>

				<div class="row">
          <div class="col-12">
            <form id="produk_ubah" enctype='multipart/form-data'>
              <input type="hidden" class="form-control" id="id" value="<?=$produk->id?>" required>
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputFile">Foto Produk</label>
                  <div class="input-group">
                    <img id="fotopreview" src="<?=cekfoto('assets/img/produk/'.$produk->slug.'.jpg')?>" style="width:100%;max-width:200px;cursor:pointer" onclick="$('#inputfoto').click()" src="<?=URLUTAMA?>assets/img/icon-uploader.jpg" onerror="this.src='<?=URLUTAMA?>assets/img/icon-uploader.jpg';" />
                    <div class="custom-file" style="display:none">
                      <input type="file" class="custom-file-input" accept="image/*" id="inputfoto" onchange="loadFile(event)">
                      <label class="custom-file-label" for="exampleInputFile">Pilih Gambar</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Produk" value="<?=$produk->nama?>" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Kategori</label>
                  <select class="form-control" id="id_kategori" name="id_kategori" required>
                    <?php foreach ($kategori as $key => $kat) {
                      $selected=($produk->id_kategori==$kat->id)?' selected':'';
                    echo '<option value="'.$kat->id.'"'.$selected.'>'.$kat->nama.'</option>';
                    } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Deskripsi</label>
                  <textarea class="form-control textarea" id="deskripsi" placeholder="Masukkan Deskripsi" required ><?=$produk->deskripsi?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga</label>
                  <input type="number" class="form-control" id="harga" placeholder="Masukkan Harga" min="1" value="<?=$produk->harga?>" required>
                </div>

              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" class="btn btn-primary" onclick="produk_ubah()">Simpan</button>
              </div>
            </form>
          </div>
				</div>
			</div>
		</section>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
  $("#id_kategori").select2();
  $('.textarea').summernote({
        placeholder: 'Deskripsi Produk',
        tabsize: 2,
        height: 200
      });
});
var loadFile = function(event) {
  var output = document.getElementById('fotopreview');
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function() {
    URL.revokeObjectURL(output.src) // free memory
  }
};
function produk_ubah(page=0,perpage=25,filter='') {
  $("#produk_ubah").validate();
  var files = $('#inputfoto')[0].files;


  if ($('#nama').val()!='' && $('#deskripsi').val()!='' && $('#id_kategori').val()!='' && $('#harga').val()!='') {
    var data = new FormData();
    data.append('id',$('#id').val());
    data.append('nama',$('#nama').val());
      data.append('id_kategori', $('#id_kategori').val());
      data.append('deskripsi', $('#deskripsi').val());
      data.append('harga', $('#harga').val());
      data.append('slug', '<?=$produk->slug?>');
      data.append('created_at', '<?=$produk->created_at?>');
      if(files.length > 0 ){
        data.append('inputfoto', files[0]);
      }
    $.ajax({
          url: "<?=URLUTAMA?>api/produk_ubah",
          type: 'post',
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
             if (data==1 || data=='success') {$('#modal-success .keterangan>p').html('Data Berhasil Diubah'); $('#modal-success').modal();
                $("#modal-success").on("hidden.bs.modal", function () {location.href = '<?=URLUTAMA?>adminproduk'; });
             } else { $('#modal-danger .keterangan>p').html(data); $('#modal-danger').modal(); }
          },
       });
   }else{
      $('#modal-danger .keterangan>p').html('Nama, Deskripsi, Kategori dan Harga Tidak Boleh Kosong'); $('#modal-danger').modal();
   }
}


</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
