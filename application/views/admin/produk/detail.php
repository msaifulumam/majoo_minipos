<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Detail Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->

          </div>
				</div>

				<div class="row">
          <div class="col-12">
              <input type="hidden"  id="id" value="<?=$produk->id?>" required>
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputFile">Foto Produk</label>
                  <div class="input-group">
                    <img id="fotopreview" src="<?=cekfoto('assets/img/produk/'.$produk->slug.'.jpg')?>" style="width:100%;max-width:200px;cursor:pointer" src="<?=URLUTAMA?>assets/img/icon-uploader.jpg" onerror="this.src='<?=URLUTAMA?>assets/img/icon-uploader.jpg';" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <p ><?=$produk->nama?></p>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Kategori</label>
                  <p ><?=$produk->kategori?></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Deskripsi</label>
                  <p ><?=$produk->deskripsi?></p>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga</label>
                  <p >Rp. <?=number_format($produk->harga, 3, '', '.')?></p>
                </div>
                <a href="<?=URLUTAMA?>/adminproduk/ubah/<?=$produk->slug?>" class="btn-success btn">Ubah</a>
              </div>

              <!-- /.card-body -->
          </div>
				</div>
			</div>
		</section>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {

});
</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
