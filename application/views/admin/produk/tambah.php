<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tambah Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->

          </div>
				</div>

				<div class="row">
          <div class="col-12">
            <form id="produk_tambah" enctype='multipart/form-data'>
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputFile">Foto Produk</label>
                  <div class="input-group">
                    <img id="fotopreview" src="" style="width:100%;max-width:200px;cursor:pointer" onclick="$('#inputfoto').click()" src="<?=URLUTAMA?>assets/img/icon-uploader.jpg" onerror="this.src='<?=URLUTAMA?>assets/img/icon-uploader.jpg';" />
                    <div class="custom-file" style="display:none">
                      <input type="file" class="custom-file-input" accept="image/*" id="inputfoto" onchange="loadFile(event)">
                      <label class="custom-file-label" for="exampleInputFile">Pilih Gambar</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Produk" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Kategori</label>
                  <select class="form-control" id="id_kategori" name="id_kategori" required>
                    <?php foreach ($kategori as $key => $kat) {
                    echo '<option value="'.$kat->id.'">'.$kat->nama.'</option>';
                    } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Deskripsi</label>
                  <textarea class="form-control textarea" id="deskripsi" placeholder="Masukkan Deskripsi" required ></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga</label>
                  <input type="number" class="form-control" id="harga" placeholder="Masukkan Harga" min="1" required>
                </div>

              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" class="btn btn-primary" onclick="produk_tambah()">Simpan</button>
              </div>
            </form>
          </div>
				</div>
			</div>
		</section>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
  $("#id_kategori").select2();
  $('.textarea').summernote({
        placeholder: 'Deskripsi Produk',
        tabsize: 2,
        height: 200
      });
});
var loadFile = function(event) {
  var output = document.getElementById('fotopreview');
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function() {
    URL.revokeObjectURL(output.src) // free memory
  }
};
function produk_tambah(page=0,perpage=25,filter='') {
  $("#produk_tambah").validate();
  var files = $('#inputfoto')[0].files;

  // Check file selected or not
  if(files.length > 0 ){
    if ($('#nama').val()!='' && $('#deskripsi').val()!='' && $('#id_kategori').val()!='' && $('#harga').val()!='') {
      var data = new FormData();
      data.append('nama',$('#nama').val());
      data.append('id_kategori', $('#id_kategori').val());
      data.append('deskripsi', $('#deskripsi').val());
      data.append('harga', $('#harga').val());
      data.append('inputfoto', files[0]);

    $.ajax({
          url: "<?=URLUTAMA?>api/produk_tambah",
          type: 'post',
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
             if (data==1 || data=='success') {$('#modal-success .keterangan>p').html('Data Berhasil Ditambahkan'); $('#modal-success').modal(); $("#modal-success").on("hidden.bs.modal", function () {location.href = '<?=URLUTAMA?>adminproduk'; });} else { $('#modal-danger .keterangan>p').html(data); $('#modal-danger').modal(); }
          },
       });
     }else{
        $('#modal-danger .keterangan>p').html('Nama, Deskripsi, Kategori dan Harga Tidak Boleh Kosong'); $('#modal-danger').modal();
     }
    }else{
       $('#modal-danger .keterangan>p').html('Pilih Foto Dulu'); $('#modal-danger').modal();
    }
}


</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
