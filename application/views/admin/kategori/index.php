<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layouts/adminhead.php');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Kategori</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Admin Kategori</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-9 col-6">
            <!-- small box -->
            <a href="<?=URLUTAMA?>adminkategori/tambah" class="btn btn-primary" >Tambah</a>
          </div>
          <div class="col-lg-3 col-6" style="text-align:right">
          <div class="form-group">
              <label>PerPage</label>
              <select id="perpage" class="form-control" onchange="gantiperpage()">
                <option value="2" selected>2</option>
                <option value="3">3</option>
                <option value="10">10</option>
                <option value="50">50</option>
              </select>
            </div>
          </div>
				</div>

				<div class="row">
          <div class="col-12">
						<table id="kategori_list" class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Foto</th>
                  <th>Nama Kategori</th>
                  <th>Deskripsi</th>
                  <th>slug</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="pagination">
              <ul class="pagination">

              </ul>
            </div>
          </div>
				</div>
			</div>
		</section>

<script type="text/javascript">
var pagenow=1, perpagenow=25, jmldata=0;
function kategori_load(page=1,perpage=25,filter='') {
  $.post( "<?=URLUTAMA?>api/kategori_get", {
    data: [''],
    select: ['id','nama','deskripsi','slug'],
    filter: filter,
    page: page-1,
    perpage: perpage
  })
  .done(function( data ) {
    data = JSON.parse(data);
    var html = '';
    pagenow=page, perpagenow=perpage;
    $.each( data, function( i, item ) {
      var linkimg = '<?=URLUTAMA?>assets/img/kategori/'+item.slug+'.jpg';
      var imsrc = imageExists(linkimg)?linkimg:'<?=URLUTAMA?>assets/img/kategori-default.jpg';
      html += '<tr><td>'+item.id+'</td><td><img src="'+imsrc+'" style="height:50px" /></td><td>'+item.nama+'</td><td>'+item.deskripsi+'</td><td>'+item.slug+'</td><td><div class="btn-group">'+
                '<button type="button" class="btn btn-success">Action</button>'+
                  '<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'+
                    '<span class="sr-only">Toggle Dropdown</span>'+
                  '</button>'+
                  '<div class="dropdown-menu" role="menu" wfd-invisible="true" style="">'+
                    '<a class="dropdown-item" href="<?=URLUTAMA?>adminkategori/detail/'+item.slug+'">Detail</a>'+
                    '<a class="dropdown-item" href="<?=URLUTAMA?>adminkategori/ubah/'+item.slug+'">Ubah</a>'+
                    '<a class="dropdown-item" href="#">Hapus</a>'+
                  '</div>'+
                '</div></td><td>';
    });
    $('#kategori_list>tbody').html(html);
    updatejmldata();
  });
}
function gantipage(page=0) {
  kategori_load(page,perpagenow);
}
function gantiperpage() {
  kategori_load(1,$('#perpage').val());
}
function updatejmldata(filter='') {
  $.post( "<?=URLUTAMA?>api/kategori_getall", {
    data: [''],
    filter: filter
  })
  .done(function( data ) {
    jmldata = JSON.parse(data);
    getpagination();
  });
}
function getpagination() {
  var jmlpage=Math.ceil(jmldata/perpagenow);
  var htmlpagination = '<li class="paginate_button page-item previous'+((pagenow==1)?' disabled':'')+'" id="paination_previous"><p '+((pagenow==1)?'':'onclick="gantipage('+(pagenow-1)+')" style="cursor:pointer"')+' aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Prev</p></li>';
  for (var i = 1; i <= jmlpage; i++) {
    htmlpagination+='<li class="paginate_button page-item '+((pagenow==i)?' active':'')+'"><p aria-controls="example2" data-dt-idx="1" tabindex="0" class="page-link" '+((pagenow==i)?'':'onclick="gantipage('+i+')" style="cursor:pointer"')+'>'+i+'</a></li>';
  }
  htmlpagination += '<li class="paginate_button page-item next'+((pagenow==jmlpage)?' disabled':'')+'" id="paination_next"><p '+((pagenow==jmlpage)?'':'onclick="gantipage('+(pagenow+1)+')"')+' '+((pagenow==jmlpage)?'':'style="cursor:pointer"')+' aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Next</p></li>';
  $('#pagination>ul').html(htmlpagination);
}
document.addEventListener("DOMContentLoaded", function(event) {
  kategori_load(1,$('#perpage').val());
});
</script>
<?php $this->load->view('layouts/adminfoot.php'); ?>
