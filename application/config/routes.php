<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api/produk_get'] = 'api/produk_get';

$route['adminproduk/ubah/(:id)'] = 'adminproduk/ubah/$1';
