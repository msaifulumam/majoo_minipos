<?php
class Kategori extends CI_Model {

        public $id;
        public $nama;
        public $deskripsi;
        public $slug;

        public function table() {
            return 'kategori';
        }

        public function get($select,$filter='',$page='0',$perpage='25')
        {
          $sel = ''; foreach ($select as $key => $data) {$sel.=$data.',';} $sel=($sel=='')?'*':substr_replace($sel ,"", -1);
          $fil=($filter=='')?'1':$filter; $page=((int)$page>=0)?$page:'0'; $perpage=((int)$perpage>0)?$perpage:'25';
          $sql = "SELECT $sel FROM ".$this->table()." WHERE $fil LIMIT $perpage OFFSET ".($page*$perpage);
        //return slug($sql);
          $query = $this->db->query($sql);
          return $query->result();
        }

        public function getall_count($filter='')
        {
          $fil=($filter=='')?'1':$filter;
          $sql = "SELECT count(id) as jml FROM ".$this->table()." WHERE $fil ";
          $query = $this->db->query($sql);
          return $query->result();
        }

        public function tambah($data,$file)
        {
            $this->nama  = $data['nama'];
            $this->deskripsi  = $data['deskripsi'];
            $this->slug  = $data['slug'];
            if (empty($this->get(['id'],'nama="'.$this->nama.'"'))) {
              if ($this->db->insert($this->table(), $this))  uploadfoto($file['inputfoto'],'kategori',$this->slug); else return 'Gagal Memasukkan Data, Silahkan Coba Lagi';
            }
            return 'Nama Sudah Ada';
        }

        public function ubah($data,$file)
        {
          $this->id  = $data['id'];
          $this->nama  = $data['nama'];
          $this->deskripsi  = $data['deskripsi'];
          $this->slug  = $data['slug'];
          if (empty($this->get(['id'],'nama="'.$this->nama.'" AND id!='.$this->id))) {
            if ($this->db->update($this->table(), $this, array('id' => $this->id ))) {if (isset($file['inputfoto'])) uploadfoto($file['inputfoto'],'kategori',$this->slug); } else return 'Gagal Memasukkan Data, Silahkan Coba Lagi';
          }
          return 'Nama Sudah Ada';
        }

}
