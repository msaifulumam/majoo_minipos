<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSupplier extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
			parent::__construct();
			if ( ! $this->session->userdata('logged_in')) {
				redirect(URLUTAMA.'home/login');
			}
	}
	public function index() {
		$this->load->view('admin/supplier/index');
	}
	public function ubah($id='') {
		$this->load->model('supplier');
		$supplier = $this->supplier->get(['id','nama','alamat','nohp'],'id="'.$id.'"',0,100)[0];
		$this->load->view('admin/supplier/ubah',['supplier'=>$supplier]);
	}
	public function tambah() {
		$this->load->view('admin/supplier/tambah');
	}
}
