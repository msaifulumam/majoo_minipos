<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminProduk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
			parent::__construct();
			if ( ! $this->session->userdata('logged_in')) {
				redirect(URLUTAMA.'home/login');
			}
	}
	public function index() {
		$this->load->view('admin/produk/index');
	}
	public function detail($slug='') {
		$this->load->model('produk');
		$produk = $this->produk->getone(['p.id','p.nama','p.deskripsi','p.id_kategori','p.slug','p.harga','p.created_at','k.nama as kategori'],'p.slug="'.$slug.'"',0,100)[0];
		$this->load->view('admin/produk/detail',['slug'=>$slug,'produk'=>$produk]);
	}
	public function ubah($slug='') {
		$this->load->model('kategori');
		$this->load->model('produk');
		$kategori = $this->kategori->get(['id','nama'],'',0,100);
		$produk = $this->produk->get(['id','nama','deskripsi','id_kategori','slug','harga','created_at'],'slug="'.$slug.'"',0,100)[0];
		$this->load->view('admin/produk/ubah',['slug'=>$slug,'kategori'=>$kategori,'produk'=>$produk]);
	}
	public function tambah() {
		$this->load->model('kategori');
		$kategori = $this->kategori->get(['id','nama'],'',0,100);
		$this->load->view('admin/produk/tambah',['kategori'=>$kategori]);
	}
}
