<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
			parent::__construct();
			if ( ! $this->session->userdata('logged_in') && $this->uri->segment('2')!='login') {
				redirect(URLUTAMA.'home/login');
			}
	}
	public $token='';
	public function validateuser($data) {

	}
	public function login() {
		$this->load->model('pengguna');
		$username = $_POST['username']??'';
		$password = md5($_POST['password'])??'';
		echo $this->pengguna->login($username,$password);
	}
	public function logout() {
		$this->session->sess_destroy();
		redirect(URLUTAMA.'home/login');
	}

	public function produk_get() {
		$this->load->model('produk');
		$select = $_POST['select']??[];
		$filter = $_POST['filter']??'';
		$page = $_POST['page']??'0';
		$perpage = $_POST['perpage']??'25';
		if (is_array($select)) echo json_encode($this->produk->get($select,$filter,$page,$perpage)); else echo 'fail';
	}

	public function produk_getall() {
		$this->load->model('produk');
		$filter = $_POST['filter']??'';
		// print_r($this->produk->getall_count($filter)[0]);
		echo json_encode($this->produk->getall_count($filter)[0]->jml);
	}

	public function produk_tambah() {
		$this->load->model('produk');
		$_POST['nama'] = $_POST['nama']??'';
		$_POST['deskripsi'] = $_POST['deskripsi']??'';
		$_POST['harga'] = $_POST['harga']??0;
		$_POST['slug'] = slug($_POST['nama']);
		if ($this->produk->tambah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
	public function produk_ubah() {
		$this->load->model('produk');
		$_POST['id'] = $_POST['id']??'';
		$_POST['nama'] = $_POST['nama']??'';
		$_POST['deskripsi'] = $_POST['deskripsi']??'';
		$_POST['harga'] = $_POST['harga']??0;
		$_POST['slug'] = $_POST['slug']??'';
		$_POST['created_at'] = $_POST['created_at']??time();
		if ($this->produk->ubah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
	public function produk_hapus() {
		$this->load->model('produk');
		$_POST['id'] = $_POST['id']??'';
		if ($this->produk->hapus($_POST['id'])) echo 'success'; else echo 'fail';
	}

	public function kategori_get() {
	  $this->load->model('kategori');
	  $select = $_POST['select']??[];
	  $filter = $_POST['filter']??'';
	  $page = $_POST['page']??'0';
	  $perpage = $_POST['perpage']??'25';
	  if (is_array($select)) echo json_encode($this->kategori->get($select,$filter,$page,$perpage)); else echo 'fail';
	}

	public function kategori_getall() {
	  $this->load->model('kategori');
	  $filter = $_POST['filter']??'';
	  // print_r($this->kategori->getall_count($filter)[0]);
	  echo json_encode($this->kategori->getall_count($filter)[0]->jml);
	}

	public function kategori_tambah() {
	  $this->load->model('kategori');
	  $_POST['nama'] = $_POST['nama']??'';
	  $_POST['deskripsi'] = $_POST['deskripsi']??'';
	  $_POST['slug'] = slug($_POST['nama']);
	  if ($this->kategori->tambah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
	public function kategori_ubah() {
	  $this->load->model('kategori');
	  $_POST['id'] = $_POST['id']??'';
	  $_POST['nama'] = $_POST['nama']??'';
	  $_POST['deskripsi'] = $_POST['deskripsi']??'';
	  $_POST['slug'] = $_POST['slug'];
	  if ($this->kategori->ubah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}



	public function pembeli_get() {
		$this->load->model('pembeli');
		$select = $_POST['select']??[];
		$filter = $_POST['filter']??'';
		$page = $_POST['page']??'0';
		$perpage = $_POST['perpage']??'25';
		if (is_array($select)) echo json_encode($this->pembeli->get($select,$filter,$page,$perpage)); else echo 'fail';
	}

	public function pembeli_getall() {
		$this->load->model('pembeli');
		$filter = $_POST['filter']??'';
		// print_r($this->pembeli->getall_count($filter)[0]);
		echo json_encode($this->pembeli->getall_count($filter)[0]->jml);
	}

	public function pembeli_tambah() {
		$this->load->model('pembeli');
		$_POST['nama'] = $_POST['nama']??'';
		$_POST['alamat'] = $_POST['alamat']??'';
		$_POST['nohp'] = $_POST['nohp']??0;
		if ($this->pembeli->tambah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
	public function pembeli_ubah() {
		$this->load->model('pembeli');
		$_POST['id'] = $_POST['id']??'';
		$_POST['nama'] = $_POST['nama']??'';
		$_POST['alamat'] = $_POST['alamat']??'';
		$_POST['nohp'] = $_POST['nohp']??0;
		if ($this->pembeli->ubah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}

	public function supplier_get() {
	  $this->load->model('supplier');
	  $select = $_POST['select']??[];
	  $filter = $_POST['filter']??'';
	  $page = $_POST['page']??'0';
	  $perpage = $_POST['perpage']??'25';
	  if (is_array($select)) echo json_encode($this->supplier->get($select,$filter,$page,$perpage)); else echo 'fail';
	}

	public function supplier_getall() {
	  $this->load->model('supplier');
	  $filter = $_POST['filter']??'';
	  // print_r($this->supplier->getall_count($filter)[0]);
	  echo json_encode($this->supplier->getall_count($filter)[0]->jml);
	}

	public function supplier_tambah() {
	  $this->load->model('supplier');
	  $_POST['nama'] = $_POST['nama']??'';
	  $_POST['alamat'] = $_POST['alamat']??'';
	  $_POST['nohp'] = $_POST['nohp']??0;
	  if ($this->supplier->tambah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
	public function supplier_ubah() {
	  $this->load->model('supplier');
	  $_POST['id'] = $_POST['id']??'';
	  $_POST['nama'] = $_POST['nama']??'';
	  $_POST['alamat'] = $_POST['alamat']??'';
	  $_POST['nohp'] = $_POST['nohp']??0;
	  if ($this->supplier->ubah($_POST,$_FILES)) echo 'success'; else echo 'fail';
	}
}
